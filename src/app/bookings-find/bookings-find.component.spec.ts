import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingsFindComponent } from './bookings-find.component';

describe('BookingsFindComponent', () => {
  let component: BookingsFindComponent;
  let fixture: ComponentFixture<BookingsFindComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingsFindComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingsFindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
