import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BookingServices} from '../services/booking.services';

@Component({
  selector: 'app-bookings-find',
  templateUrl: './bookings-find.component.html',
  styleUrls: ['./bookings-find.component.scss']
})
export class BookingsFindComponent implements OnInit {
  bookid: string;
  info: any;
  constructor(private activeroute: ActivatedRoute, private bookingservices: BookingServices) {
    this.bookid = this.activeroute.snapshot.paramMap.get('id');
    if (this.bookid) {
      this.bookingservices.getBooking(this.bookid).subscribe(res => {
        this.info = res;
      });
    }
  }
  ngOnInit() {
  }
  searchBooking() {
    this.info = undefined;
    this.bookingservices.getBooking(this.bookid).subscribe(res => {
      this.info = res;
    });
  }
}
