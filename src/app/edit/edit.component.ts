import { Component, NgZone, ElementRef, ViewChild } from '@angular/core';
import { HotelsServices } from '../services/hotels.services';
import { ActivatedRoute } from '@angular/router';
import {Description, DescriptionEdit, HotelEdit} from '../hotels/hotels.classes';
import { MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';

declare var google: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent {
  hotelId: number;
  hotelTypes: any[] = [];
  hotelDescription = new Description();
  hotelInfo = new HotelEdit();
  hotelDescriptionEdit = new DescriptionEdit();
  @ViewChild('search')
  public searchElementRef: ElementRef;
  constructor(private hotelservices: HotelsServices, private activeroute: ActivatedRoute, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
    this.hotelId = Number(this.activeroute.snapshot.paramMap.get('id'));
    this.hotelservices.getHotelInfoForEdit(this.hotelId).subscribe(res => {
      this.hotelInfo.init(res);
    });
    this.hotelservices.getHotelTypes().subscribe(res => {
      this.hotelTypes = res;
    });
    this.hotelservices.getHotelUserDescription(this.hotelId).subscribe(res => {
      this.hotelDescription.init(res);
    });
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.hotelInfo.lat = place.geometry.location.lat();
          this.hotelInfo.lng = place.geometry.location.lng();
          this.setNewLocation(this.hotelInfo.lat, this.hotelInfo.lng);
        });
      });
    });
  }
  saveHotelInfo() {
    this.hotelservices.updateHotelInfo(this.hotelId, this.hotelInfo).subscribe();
  }
  saveDescription() {
    this.hotelservices.updateHotelDescription(this.hotelId, this.hotelDescriptionEdit).subscribe();
  }
  mapClicked($event) {
    this.hotelInfo.lng = $event.coords.lng;
    this.hotelInfo.lat = $event.coords.lat;
    this.setNewLocation(this.hotelInfo.lat, this.hotelInfo.lng);
  }
  setNewLocation(latitude: number, longitude: number) {
    const latlng = new google.maps.LatLng(latitude, longitude);
    const request = { latLng: latlng };
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode(request, (results, status) => {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[0] !== null && results[0] !== undefined) {
          console.log(results);
          this.hotelInfo.location.cityId = results.find(x => x.types[0] === 'locality').place_id;
          this.hotelInfo.location.regionId = results.find(x => x.types[0] === 'administrative_area_level_1').place_id;
          const country = results.find(x => x.types[0] === 'country');
          this.hotelInfo.country = country.address_components[0].short_name;
          if (this.hotelInfo.location.regionId !== undefined) {
            this.hotelInfo.location.address = results[0].formatted_address;
          }
        }
      }
    });
  }
}
