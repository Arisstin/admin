import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthServices } from './auth.services';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import {Router} from '@angular/router';


@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor(private auth: AuthServices, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // console.log(req.headers.has('Authorization'));
    if (req.headers.has('Authorization')) {
      req = req.clone({  setHeaders: {
          Authorization: `Bearer ${this.auth.getHotelToken()}`
        }});
    } else {
      req = req.clone({  setHeaders: {
          Authorization: `Bearer ${this.auth.getAdminToken()}`
        }});
    }
    return next.handle(req)
      .catch((error, caught) => {
        if (error.status === 401) {
          this.auth.cancelAuth();
          this.router.navigate(['login']);
          this.auth.isAuthorized.next(undefined);
        }
        return Observable.throw(error);
      }) as any;
  }
}
