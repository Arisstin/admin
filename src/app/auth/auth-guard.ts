import { Injectable } from '@angular/core';
import { Router, CanActivate,  ActivatedRouteSnapshot } from '@angular/router';
import { AuthServices } from './auth.services';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthServices, public router: Router) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    console.log(this.auth.isAuthorized.value);
    if (!this.auth.isAuthorized.value || (route.data && route.data.role !== this.auth.isAuthorized.value)) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
