import { Injectable,  } from '@angular/core';
import { BaseService } from '../core/base';
import { LoginForm } from '../login/login.classes';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpRequest } from '@angular/common/http';

@Injectable()
export class AuthServices extends BaseService {
  isAuthorized: BehaviorSubject<string> = new BehaviorSubject<string>(undefined);

  cachedRequests: Array<HttpRequest<any>> = [];

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    // retry the requests. this method can
    // be called after the token is refreshed
    this.cachedRequests.forEach(r => r.clone());
  }
  public logining(form: LoginForm): Observable<any> {
    return this.post('admin/login', form);
  }
  public setAdminToken(token: string) {
    localStorage.setItem('token_admin', token);
  }
  public getAdminToken() {
    return localStorage.getItem('token_admin');
  }
  public cancelAuth() {
    localStorage.removeItem('token_admin');
    localStorage.removeItem('token_hotel');
  }
  public setHotelToken(token: string) {
    localStorage.setItem('token_hotel', token);
  }
  public getHotelToken() {
    return localStorage.getItem('token_hotel');
  }
}
