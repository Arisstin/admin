import {Component, Input, OnInit} from '@angular/core';
import {
  faCoffee,
  faBicycle,
  faBriefcaseMedical,
  faDoorOpen,
  faIndustry,
  faPaw,
  faSwimmingPool,
  faTree,
  faWifi,
  faUmbrellaBeach,
  faSubway,
  faSnowflake,
  faStar,
  faPercent,
  faParking,
  faBlind,
  faHelicopter,
  faFootballBall,
  faChessKnight,
  faBed,
  faRoad,
  faPlane,
  faBan
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-hotels-theme-icon',
  templateUrl: './hotel-theme-icon.component.html'
})
export class HotelThemeIconComponent implements OnInit {
  @Input() name: string;
  @Input() active: boolean;
  tooltip = '';
  icon = faCoffee;

  ngOnInit() {
    switch (this.name) {
      case 'bicycle' : { this.icon = faBicycle; this.tooltip = 'велосиедные дорожки'; break; }
      case 'health' : { this.icon = faBriefcaseMedical; this.tooltip = 'медицинская помощь'; break; }
      case 'open' : { this.icon = faDoorOpen; this.tooltip = 'открытый вход'; break; }
      case 'industry' : { this.icon = faIndustry; this.tooltip = 'рядом промышленный район'; break; }
      case 'animals' : { this.icon = faPaw; this.tooltip = 'разрешены животные'; break; }
      case 'pool' : { this.icon = faSwimmingPool; this.tooltip = 'есть басейн'; break; }
      case 'forest' : { this.icon = faTree; this.tooltip = 'рядом лес или парковая зона'; break; }
      case 'inthernet' : { this.icon = faWifi; this.tooltip = 'есть доступный интернет'; break; }
      case 'beach' : { this.icon = faUmbrellaBeach; this.tooltip = 'рядом пляж'; break; }
      case 'subway' : { this.icon = faSubway; this.tooltip = 'рядом железнодорожная станция'; break; }
      case 'snow' : { this.icon = faSnowflake; this.tooltip = 'рядом лыжный курорт'; break; }
      case 'popular' : { this.icon = faStar; this.tooltip = 'популярный'; break; }
      case 'econom' : { this.icon = faPercent; this.tooltip = 'бюджетный вариант'; break; }
      case 'parking' : { this.icon = faParking; this.tooltip = 'доступна парковка'; break; }
      case 'invalid' : { this.icon = faBlind; this.tooltip = 'для людей с ограниченными возможностями'; break; }
      case 'helicopter' : { this.icon = faHelicopter; this.tooltip = 'есть вертолетная площадка'; break; }
      case 'footbal' : { this.icon = faFootballBall; this.tooltip = 'рядом стадион'; break; }
      case 'horses' : { this.icon = faChessKnight; this.tooltip = 'рядом конюшни'; break; }
      case 'bed' : { this.icon = faBed; this.tooltip = 'спальник'; break; }
      case 'road' : { this.icon = faRoad; this.tooltip = 'придорожный отель'; break; }
      case 'plane' : { this.icon = faPlane; this.tooltip = 'рядом аэропорт'; break; }
      case 'cafe' : { this.icon = faCoffee; this.tooltip = 'есть своя кухня или кафе'; break; }
      default : { this.icon = faBan; this.tooltip = name; break; }
    }
  }
}
