export class DataInfoPage {
  id?: number;
  seo: string;
  title_en: string;
  title_ru: string;
  title_ua: string;
  content_ua: any;
  content_en: any;
  content_ru: any;
  constructor() {
    this.seo = '';
    this.title_ru = '';
    this.title_en = '';
    this.title_ua = '';
    this.content_ua = '';
    this.content_en = '';
    this.content_ru = '';
  }
}
