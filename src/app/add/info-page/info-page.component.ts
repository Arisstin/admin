import { Component, OnInit } from '@angular/core';
import {DataInfoPage} from './info-page.classes';
import {InfoServices} from '../../services/info.services';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-info-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit {
  data: DataInfoPage;
  options: any;
  constructor(private infoservices: InfoServices, private activeroute: ActivatedRoute) {
    this.data = new DataInfoPage();
  this.options = {
    charCounterCount: false,
    language: 'ru'
    };
  const infomod = this.activeroute.snapshot.paramMap.get('id');
  if (infomod) {
    this.infoservices.getInfoPageInfo(infomod).subscribe(res => {
      this.data = res;
    });
  }
  }

  ngOnInit() {
  }
  deleteInfoPage() {
    this.infoservices.deleteInfoPage(this.data.id).subscribe();
  }
  onSubmit() {
    this.infoservices.postInfoPage(this.data).subscribe(res => {
      this.data.id = res.id;
    });
  }
}
