export class SeoInfo {
  id ?: number;
  url: string;
  type: string;
  hb_id: string;
  data: any;
  title_ua: string;
  title_en: string;
  title_ru: string;
  meta_ua: string;
  meta_en: string;
  meta_ru: string;
  description_ua: string;
  description_en: string;
  description_ru: string;
  constructor() {
    this.hb_id = '';
    this.data = [];
    this.type = 'country';
    this.title_ua = '';
    this.title_en = '';
    this.title_ru = '';
    this.url = '';
    this.meta_ua = '';
    this.meta_en = '';
    this.meta_ru = '';
    this.description_ua = '';
    this.description_en = '';
    this.description_ru = '';
  }
}
