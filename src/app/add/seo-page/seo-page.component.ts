import {Component} from '@angular/core';
import {InfoServices} from '../../services/info.services';
import {SeoInfo} from './seo-page.classes';
import {} from '@types/googlemaps';

@Component({
  selector: 'app-seo-page',
  templateUrl: './seo-page.component.html',
  styleUrls: ['./seo-page.component.scss']
})
export class SeoPageComponent  {
  data: SeoInfo;
  dataseo: any[] = [];
  showEdit = false;
  editId: number;
  typeEnum: string[];
  destinationsList = [];
  hidelist = true;
  countries: any[] = [];
  constructor(private infoservices: InfoServices) {
    this.typeEnum = ['country', 'region', 'city', 'theme'];
    this.loadAll();
    this.infoservices.getCountriesList().subscribe(res => { this.countries = res; });
  }
  openEdit(index: number) {
    this.destinationsList = this.countries;
    this.hidelist = true;
    this.data = new SeoInfo();
    this.editId = this.dataseo[index].id;
    this.infoservices.getSeoInfobyId(this.editId).subscribe(res => {
      this.data = res;
      this.showEdit = true;
    });
  }
  createNew() {
    this.destinationsList = this.countries;
    this.hidelist = true;
    this.data = new SeoInfo();
    this.showEdit = true;
  }
  loadAll() {
    this.infoservices.getSeoInfo().subscribe(res => {
      this.dataseo = res;
      this.editId = undefined;
      this.showEdit = false;
    });
  }
  deleteSeobyId(id: number) {
    this.infoservices.deleteSeoInfo(id).subscribe(() => {
      this.showEdit = false;
      const ind = this.dataseo.findIndex(x => x.id === id);
      this.dataseo.splice(ind, 1);
      this.editId = undefined;
    });
  }
  deleteSeobyIndex(index: number) {
    this.infoservices.deleteSeoInfo(this.dataseo[index].id).subscribe(() => {
      this.dataseo.splice(index, 1);
    });
  }
  onSubmit() {
    if (this.editId) {
      this.infoservices.updateSeoInfo(this.editId, this.data).subscribe(() => {
        this.loadAll();
      });
    } else {
      this.infoservices.postSeoInfo(this.data).subscribe(() => {
        this.loadAll();
      });
    }
  }
  typeChange() {
    this.data.hb_id = '';
  }
  clickHBid(value: string, lvl: string) {
    if (this.data.type === 'country') {
        this.data.hb_id = value;
    } else {
      if (this.destinationsList === this.countries) {
          this.destinationsList = [];
          this.infoservices.getDestinationsList(value).subscribe(res => { this.destinationsList = res; });
      } else {
        if (lvl === this.data.type) {
          this.data.hb_id = value;
        }
      }
    }
  }
  lvlup() {
    this.destinationsList = this.countries;
  }
}
