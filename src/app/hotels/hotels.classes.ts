import {moment} from 'ngx-bootstrap/chronos/test/chain';
export class ObjectType {
  id: number;
  title: string;
}
export class Token {
  token: string;
}
export class SearchForm {
  from: Date;
  to: Date;
  title: string;
  status: string;
  type: number;
  themes: number[];
  limit: number;
  offset: number;
  constructor() {
    // this.from = new Date();
    // this.to = new Date();
    // this.to.setDate(this.from.getDate() + 30);
    this.limit = 25;
    this.offset = 0;
    this.from = new Date(2017, 5, 10);
    this.to = new Date();
    this.title = '';
    this.status = '';
    this.type = 0;
    this.themes = [];
  }
  public convert() {
    const _data = {
      from: moment(this.from).format('YYYY-MM-DD'),
      to: moment(this.to).format('YYYY-MM-DD'),
      title: this.title,
      type: this.type,
      status: this.status,
      limit: this.limit,
      offset: this.offset
    };
    return _data;
  }
}
export class Hotel {
  id: number;
  country: Country;
  title: string;
  user: Registration;
  type: Type;
  stars: number;
  star?: number[];
  token: string;
  status: string;
  themes: number[];
}
export class HotelEdit {
  titleEN: string;
  titleRU: string;
  titleUA: string;
  stars: number;
  lat: number;
  lng: number;
  type: Type;
  location: LocationEdit;
  email: string;
  country: string;
  constructor() {
    this.titleEN = '';
    this.titleRU = '';
    this.titleUA = '';
    this.stars = 0;
    this.lat = 0;
    this.lng = 0;
    this.email = '';
    this.type = new Type();
    this.country = '';
    this.location = new LocationEdit();
  }
  public init(data: any) {
    this.titleEN = data.titleEN;
    this.titleUA = data.titleUA;
    this.titleRU = data.titleRU;
    this.lng = data.location.lng;
    this.lat = data.location.lat;
    this.location.address = data.location.address;
    this.location.regionId = data.location.regionId;
    this.location.cityId = data.location.cityId;
    this.country = data.country.id;
    this.type = data.type;
    this.stars = data.stars;
    this.email = data.user.email;
  }
}
export class LocationEdit {
  address: string;
  cityId: string;
  regionId: string;
  constructor() {
    this.address = '';
    this.cityId = '';
    this.regionId = '';
  }
}
export class Location {
  lat: number;
  lng: number;
  address: string;
  cityId: string;
  regionId: string;
  constructor() {
    this.lat = 0;
    this.lng = 0;
    this.address = '';
    this.cityId = '';
    this.regionId = '';
  }
}
export class OpenHotelTheme {
  index: number;
  title: string;
  constructor(index: number, title: string) {
    this.index = index;
    this.title = title;
  }
}
class Country {
  title: string;
  id: number;
  constructor() {}
}
class Type {
  title: string;
  id: number;
  constructor() {}
}
class Registration {
  registrationDate: string;
}
export class Description {
  location: string;
  rooms: string;
  nutrition: string;
  environment: string;
  constructor() {
    this.location = '';
    this.environment = '';
    this.nutrition = '';
    this.rooms = '';
  }
  public init(data: any) {
    if (data) {
      this.environment = data.environment;
      this.rooms = data.rooms;
      this.nutrition = data.nutrition;
      this.location = data.location;
    }
  }
}
export class DescriptionEdit {
  locationRu: string;
  roomsRu: string;
  nutritionRu: string;
  environmentRu: string;
  locationEn: string;
  roomsEn: string;
  nutritionEn: string;
  environmentEn: string;
  locationUa: string;
  roomsUa: string;
  nutritionUa: string;
  environmentUa: string;
  constructor() {
    this.locationRu = '';
    this.environmentRu = '';
    this.nutritionRu = '';
    this.roomsRu = '';
    this.locationEn = '';
    this.environmentEn = '';
    this.nutritionEn = '';
    this.roomsEn = '';
    this.locationUa = '';
    this.environmentUa = '';
    this.nutritionUa = '';
    this.roomsUa = '';
  }
}
