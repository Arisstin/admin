import { Component, OnInit } from '@angular/core';
import { HotelsServices } from '../services/hotels.services';
import {Hotel, ObjectType, OpenHotelTheme, SearchForm} from './hotels.classes';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import {AuthServices} from '../auth/auth.services';
import {Router} from '@angular/router';
import {Globals} from '../core/globals';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faCoffee, faStar } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {
  hotelslist: Hotel[];
  themeList = [];
  hotelTypes: ObjectType[];
  mindate: Date;
  searchForm = new SearchForm();
  page: number;
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  text1: string;
  text2: string;
  isCopied1: boolean;
  isCopied2: boolean;
  faCoffee = faCoffee;
  openhoteltheme: OpenHotelTheme;
  openModalHotelTheme = false;
  plusIcon = faPlusCircle;
  starIcon = faStar;
  limitList = [25, 50, 75, 100];
  paginatin = [];
  constructor(private hotelsservice: HotelsServices, private auth: AuthServices, private router: Router, private globals: Globals) {
    this.page = 1;
  }
  ngOnInit() {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme, showWeekNumbers: false, dateInputFormat: 'DD MM YYYY' });
    this.gethotelList();
    this.hotelsservice.getHotelTypes().subscribe(res => { this.hotelTypes = res; });
    this.hotelsservice.getThemesList().subscribe(res => { this.themeList = res; }, err => {
      this.themeList = [{id: 1, title: 'bicycle'}, {id: 2, title: 'health'}, {id: 3, title: 'forest'}, {id: 4, title: 'snow'},
        {id: 5, title: 'pool'}, {id: 6, title: 'popular'}, {id: 7, title: 'footbal'}, {id: 8, title: 'parking'},
        {id: 9, title: 'inthernet'}, {id: 10, title: 'horses'}, {id: 11, title: 'plane'}, {id: 12, title: 'econom'},
        {id: 13, title: 'bed'}, {id: 14, title: 'road'}, {id: 15, title: 'beach'}, {id: 16, title: 'animals'},
        {id: 17, title: 'cafe'}];
    });
  }
  onchangeLimit() {
    this.gethotelList();
  }
  gethotelList() {
    this.hotelslist = undefined;
    this.hotelsservice.getHotelList(this.searchForm.convert()).subscribe(res => {
      this.hotelslist = res;
      this.hotelslist.forEach((_, index) => {
        this.hotelslist[index]['themes'] = [];
        this.hotelslist[index]['star'] = new Array(this.hotelslist[index].stars).fill(1);
      });
    });
  }
  openTheme(hotelindex: number) {
    this.openhoteltheme = new OpenHotelTheme(hotelindex, this.hotelslist[hotelindex].title);
    this.openModalHotelTheme = true;
  }
  searchTheme(id: number) {
    if (this.searchForm.themes.includes(id)) {
      this.searchForm.themes.splice(this.searchForm.themes.indexOf(id), 1);
    } else {
      this.searchForm.themes.push(id);
    }
  }
  HotelTheme(id: number) {
    if (this.hotelslist[this.openhoteltheme.index].themes.includes(id)) {
      // this.hotelsservice.removeTheme(this.hotelslist[this.openhoteltheme.index].id, id).subscribe();
      this.hotelslist[this.openhoteltheme.index].themes.splice(this.hotelslist[this.openhoteltheme.index].themes.indexOf(id), 1);
    } else {
      // this.hotelsservice.addTheme(this.hotelslist[this.openhoteltheme.index].id, id).subscribe();
      this.hotelslist[this.openhoteltheme.index].themes.push(id);
    }
  }
  themeTitle(id: number) {
    const ind = this.themeList.find(x => x.id === id);
    return ind.title;
  }
  goHotel() {
    window.open(this.globals.EXTRANET + 'admin', '_blank');
  }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate());
      this.mindate = day;
      if (value > this.searchForm.to) {
        this.searchForm.to = day;
      }
    }
  }
  hotelConfirm(id: number, index: number) {
    this.hotelsservice.hotelConfirm(id).subscribe(res => {
      this.hotelslist[index].status = 'CONFIRMED';
    });
  }
  hotelBlocked(id: number, index: number) {
    this.hotelsservice.hotelBlock(id).subscribe(res => {
      this.hotelslist[index].status = 'BLOCKED';
    });
  }
  openHotel(id: number, i: number) {
    this.hotelsservice.openHotel(id).subscribe(res => {
      this.auth.setHotelToken(res.token);
      this.hotelslist[i].token = res.token;
      // this.copy(res.token);
      // window.open(this.globals.EXTRANET, '_blank');
      // this.router.navigate(['/hotel']);
    });
  }
  copy(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy', false, val);
    document.body.removeChild(selBox);
  }
  hotelEdit(id: number) {
    this.router.navigate(['edit', id]);
  }
  onSubmit() {
    this.gethotelList();
  }
}
