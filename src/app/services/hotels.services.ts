import { BaseService } from '../core/base';
import { Observable } from 'rxjs/Observable';
import { Injectable,  } from '@angular/core';
import {Description, DescriptionEdit, ObjectType, Token} from '../hotels/hotels.classes';

@Injectable()
export class HotelsServices extends BaseService {
  getHotelList(data: any): Observable<any> {
    return this.get('admin/hotels', data);
  }
  getHotelTypes(): Observable<ObjectType[]> {
    return this.get('hotel-types');
  }
  openHotel(id: number): Observable<Token> {
    return this.post('admin/extranet-token', { hotelId: id });
  }
  getHotelInfo(): Observable<any> {
    return this.get_h('hotel');
  }
  hotelConfirm(id: number): Observable<any> {
    return this.post('admin/hotels/' + id + '/confirm', {});
  }
  hotelBlock(id: number): Observable<any> {
    return this.post('admin/hotels/' + id + '/block', {});
  }
  getThemesList(): Observable<any> {
    return this.get('themes');
  }
  addTheme(hotelId: number, themeId: number): Observable<any> {
    return this.post('hotel/' + hotelId + '/theme/add/' + themeId, {});
  }
  removeTheme(hotelId: number, themeId: number): Observable<any> {
    return this.post('hotel/' + hotelId + '/theme/remove' + themeId, {});
  }
  getHotelInfoForEdit(hotelId: number): Observable<any> {
    return this.get('admin/hotels/' + hotelId);
  }
  updateHotelInfo(id: number, data: any): Observable<any> {
    return this.post('admin/hotels/' + id + '/update', data);
  }
  getHotelUserDescription(id: number): Observable<Description> {
    return this.get('admin/hotels/' + id + '/description-request');
  }
  updateHotelDescription(id: number, data: DescriptionEdit): Observable<any> {
    return this.post('admin/hotels/' + id + '/add-description', data);
  }
}
