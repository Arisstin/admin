import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { BaseService } from '../core/base';

@Injectable()
export class InfoServices extends BaseService {
  postInfoPage(data: any): Observable<any> {
    return this.post('infopage', data);
  }
  deleteInfoPage(id: number): Observable<any> {
    return this.post('deleteinfo/' + id, {});
  }
  getInfoPageInfo(id: string): Observable<any> {
    return this.get('info/' + id);
  }
  getInfoPagesList(): Observable<any> {
    return this.get('infolist');
  }
  postSeoInfo(data: any): Observable<any> {
    return this.post('admin/seo', data);
  }
  getSeoInfo(): Observable<any> {
    return this.get('admin/seo');
  }
  updateSeoInfo(id: number, data: any): Observable<any> {
    return this.post('admin/seo/' + id + '/update', data);
  }
  deleteSeoInfo(id: number): Observable<any> {
    return this.get('admin/seo/' + id + '/delete');
  }
  getSeoInfobyId(id: number): Observable<any> {
    return this.get('admin/seo/' + id);
  }
  getDestinationsList(value: string): Observable<any> {
    return this.get('admin/seo/countries/' + value);
  }
  getCountriesList(): Observable<any> {
    return this.get('admin/seo/countries');
  }
}
