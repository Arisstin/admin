import { BaseService } from '../core/base';
import { Injectable,  } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { BookingPost, Bookings } from '../bookings/bookings.classes';

@Injectable()
export class BookingServices extends BaseService {
  getBooking(id: string): Observable<any> {
    return this.get('admin/bookings/' + id );
  }
  getBookings(bookInfo: BookingPost): Observable<Bookings> {
    const params = this.convertObjectToParms(bookInfo);
    return  this.get('admin/bookings?' + params);
  }
  getBookings_hb(bookInfo: BookingPost): Observable<Bookings> {
    const params = this.convertObjectToParms(bookInfo);
    return  this.get('admin/bookings/bb?' + params);
  }
  convertObjectToParms(obj: any) {
    const params = new URLSearchParams();
    for (const key in obj) {
      params.set(key, obj[key]);
    }
    return params;
  }
}
