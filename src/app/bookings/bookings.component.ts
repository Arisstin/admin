import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { BookingPost, Bookings, SearchForm } from './bookings.classes';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
import {BookingServices} from '../services/booking.services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit, AfterViewInit {
  notificate: any;
  dtOptions: DataTables.Settings = {};
  searchForm: SearchForm;
  bookingPostInfo: BookingPost;
  bookings: Bookings;
  minDate = new Date();
  minDateEdit = new Date();
  colorTheme = 'theme-dark-blue';
  bsConfig: Partial<BsDatepickerConfig>;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  tableLenght: number;
  searchParams = false;
  searchHB = false;
  constructor(private bookingservices: BookingServices,
              private router: Router) {
    this.minDateEdit.setDate(this.minDate.getDate() + 1);
    this.searchForm = new SearchForm();
    console.log(this.searchForm);
    localStorage.removeItem('confirmWindows');
    this.bookingPostInfo = new BookingPost(this.searchForm, 0, 1);
  }
  ngOnInit(): void {
    const that = this;
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme, showWeekNumbers: false, dateInputFormat: 'dd, D MMMM YYYY' });
    this.dtOptions = {
      pagingType: 'first_last_numbers',
      lengthMenu:  [ [50, 100, 150, 200, -1], [50, 100, 150, 200, 'All'] ],
      searching: false,
      // ordering: false,
      serverSide: true,
      info: false,
      columns: [
        { data: 'id', orderable: false },
        { data: 'booking' },
        { data: 'check_in' },
        { data: 'check_out' },
        { data: 'booker', orderable: false },
        { data: 'guests', orderable: false },
        { data: 'status', orderable: false },
        { data: 'price_book', orderable: false },
        { data: 'price_noshow', orderable: false },
        { data: 'price_cancel', orderable: false },
        { data: 'price_commosion', orderable: false },
        { data: 'commission', orderable: false }],
      ajax: (dataTablesParameters: any, callback) => {
        console.log(dataTablesParameters, 'ok');
        this.bookingPostInfo.limit = dataTablesParameters.length;
        this.bookingPostInfo.offset = dataTablesParameters.start;
        if (dataTablesParameters.order[0].column !== 0) {
          this.bookingPostInfo.sort_by = dataTablesParameters.columns[dataTablesParameters.order[0].column].data;
          this.bookingPostInfo.order = dataTablesParameters.order[0].dir;
        }
        if (this.bookingPostInfo.limit === -1) {
          delete this.bookingPostInfo.limit;
          this.bookingPostInfo.offset = 0;
        }
        this.tableLenght = dataTablesParameters.length;
        if (this.searchHB) {
          this.bookingservices.getBookings_hb(this.bookingPostInfo).subscribe(res => {
            that.bookings = res;
            callback({
              recordsTotal: res.total,
              recordsFiltered: res.total,
              data: []
            });
          });
        } else {
          this.bookingservices.getBookings(this.bookingPostInfo).subscribe(res => {
            that.bookings = res;
            callback({
              recordsTotal: res.total,
              recordsFiltered: res.total,
              data: []
            });
          });
        }
      }
    };
  }
  dateChanged(value) {
    if (value !== null) {
      const day = new Date();
      day.setFullYear(value.getFullYear(), value.getMonth(), value.getDate() + 1);
      this.minDate = day;
      if (value > this.searchForm.to ||
        (value.getDate() === this.searchForm.to.getDate()
          && value.getMonth() === this.searchForm.to.getMonth()
          && value.getFullYear() === this.searchForm.to.getFullYear())) {
        this.searchForm.to = day;
      }
    }
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  onSubmit() {
    console.log(this.searchHB);
    this.bookings = {total: 0, booking: []};
    localStorage.setItem('searchParams', JSON.stringify(this.searchForm.convert()));
    this.bookingPostInfo = new BookingPost(this.searchForm, 0, this.tableLenght);
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      // this.dtOptions.pageLength = 15;
      this.dtTrigger.next();
    });
  }
  openFullInfo(id: number) {
    this.router.navigate(['booking', id]);
  }
}
