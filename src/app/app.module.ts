import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Globals } from './core/globals';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { HotelsComponent } from './hotels/hotels.component';
import { LoginComponent } from './login/login.component';
import { routes } from './core/rout';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyHttpInterceptor } from './auth/httpInterceptor';
import { FormsModule } from '@angular/forms';
import { BaseService } from './core/base';
import { AuthServices } from './auth/auth.services';
import { HotelsServices } from './services/hotels.services';
import { BsDatepickerModule, defineLocale, ruLocale, PaginationModule, TabsModule } from 'ngx-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AddMenuComponent } from './menu/add-menu/add-menu.component';
import { InfoPageComponent } from './add/info-page/info-page.component';
import { InfoServices } from './services/info.services';
import {HotelThemeIconComponent} from './hotels-theme-icon/hotel-theme-icon.component';
import { EditComponent } from './edit/edit.component';
import { AgmCoreModule } from '@agm/core';
import { DataTablesModule } from 'angular-datatables';
import { BookingsComponent } from './bookings/bookings.component';
import { BookingsFindComponent } from './bookings-find/bookings-find.component';
import {BookingServices} from './services/booking.services';
import {SeoPageComponent} from './add/seo-page/seo-page.component';
import { UsersComponent } from './users/users.component';
import { UsersServices } from './services/users.services';
import { AuthGuardService } from './auth/auth-guard';

defineLocale('ru', ruLocale);

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HotelsComponent,
    LoginComponent,
    AddMenuComponent,
    InfoPageComponent,
    HotelThemeIconComponent,
    EditComponent,
    BookingsComponent,
    BookingsFindComponent,
    SeoPageComponent,
    UsersComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    ClipboardModule,
    HttpClientModule,
    PaginationModule.forRoot(),
    RouterModule.forRoot(routes),
    BsDatepickerModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    TabsModule.forRoot(),
    FontAwesomeModule,
    DataTablesModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBSbgGePitwHSVOpd0xnq5dYJcXIhyjy-A',
      libraries: ['places']
    })
  ],
  providers: [
    AuthGuardService,
    Globals,
    HotelsServices,
    BaseService,
    AuthServices,
    InfoServices,
    BookingServices,
    UsersServices,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
