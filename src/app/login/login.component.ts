import { Component, OnInit } from '@angular/core';
import {LoginForm} from './login.classes';
import { AuthServices } from '../auth/auth.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = new  LoginForm();
  constructor(private auth: AuthServices, private router: Router) {
  }

  ngOnInit() {
  }
  submitLoginForm() {
    this.auth.logining(this.loginForm).subscribe(
      token => {
        if (!token.message && token.token) {
          this.auth.setAdminToken(token.token);
          this.auth.isAuthorized.next('admin');
          // this.auth.isAuthorized.next('manager');
          this.router.navigate( ['/hotels']);
          // this.router.navigate( ['/booking']);
        }
      }
    );
  }
}
