import { Routes } from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {HotelsComponent} from '../hotels/hotels.component';
import {AddMenuComponent} from '../menu/add-menu/add-menu.component';
import {InfoPageComponent} from '../add/info-page/info-page.component';
import {EditComponent} from '../edit/edit.component';
import {BookingsComponent} from '../bookings/bookings.component';
import {BookingsFindComponent} from '../bookings-find/bookings-find.component';
import {SeoPageComponent} from '../add/seo-page/seo-page.component';
import {
  AuthGuardService as AuthGuard
} from '../auth/auth-guard';
import {UsersComponent} from '../users/users.component';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent },
  { path: 'hotels',
    component: HotelsComponent,
    canActivate: [AuthGuard],
    data: { role: 'admin' }
  },
  { path: 'edit/:id',
    component: EditComponent,
    canActivate: [AuthGuard],
    data: { role: 'admin' }
  },
  { path: 'add', component: AddMenuComponent,
    canActivate: [AuthGuard],
    data: { role: 'admin' },
    children: [
      { path: '', redirectTo: 'info-page', pathMatch: 'full' },
      { path: 'info-page', component: InfoPageComponent },
      { path: 'info-page/:id', component: InfoPageComponent },
      { path: 'seo', component: SeoPageComponent }
    ]
  },
  { path: 'bookings',
    component: BookingsComponent,
    canActivate: [AuthGuard],
    data: { role: 'admin' }
  },
  { path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: { role: 'admin' }
  },
  { path: 'booking', component: BookingsFindComponent },
  { path: 'booking/:id', component: BookingsFindComponent },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

