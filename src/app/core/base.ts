import { Injectable } from '@angular/core';
import { Headers, Response} from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Globals } from './globals';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class BaseService {

  baseUrl: string;
  api = 'extranet';
  locale = 'ru';

  constructor(
    private http: HttpClient,
    private globals: Globals
  ) {
    this.baseUrl = this.globals.BACK_END;
  }
  get(url: string, data?: any): Observable<any> {
    return this.http.get<any>(this.globals.BACK_END + this.locale + '/' + url, {params: data});
  }
  get_locale(url: string, locale: string): Observable<any> {
      return this.http.get<any>(this.globals.BACK_END + locale + '/' + url);
  }
  post(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.globals.BACK_END + this.locale + '/' + url, data);
  }
  put(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.globals.BACK_END + this.locale + '/' + url, data);
  }
  get_h(url: string): Observable<any> {
    return this.http.get<any>(this.getBaseUrl() + url, {headers: {Authorization: `Bearer ${this.getTokenHotel()}`}});
  }
  post_h(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.getBaseUrl() + url, data, {headers: {Authorization: `Bearer ${this.getTokenHotel()}`}});
  }

  put_h(url: string, data: any): Observable<any> {
    return this.http.post<any>(this.getBaseUrl() + url, data, {headers: {Authorization: `Bearer ${this.getTokenHotel()}`}});
  }

  getBaseUrl(): string {
    return this.baseUrl + this.locale + '/' + this.api + '/';
  }
  getTokenAdmin() {
    return localStorage.getItem('token_admin');
  }
  getTokenHotel() {
    return localStorage.getItem('token_hotel');
  }
}
