import { Injectable } from '@angular/core';

Injectable()
export class Globals {
  EXTRANET = 'https://extranet.bronirovka.ua/ru/login/';
  FRONT_END = 'https://bronirovka.ua';
  BACK_END = 'https://api.bronirovka.ua/';
  IMAGE_URL = 'https://images.bronirovka.ua/';
  // FRONT_END = 'http://192.168.0.103:4200';
  // BACK_END = 'http://localhost:8000/';
  // IMAGE_URL = 'http://192.168.0.100/';
  // FRONT_END = 'http://localhost:8000';
  // BACK_END = 'http://localhost:8000/';
  // IMAGE_URL = 'http://localhost/';
  HOTEL_LIMIT = 5;
  // JOIN_HREF = 'http://localhost:4201/';
}
