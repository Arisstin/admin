import { Component, OnInit} from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';
import {AuthServices} from './auth/auth.services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  outlogin = true;
  constructor(private router: Router, public auth: AuthServices) {
    if (this.auth.getAdminToken()) {
      this.auth.isAuthorized.next('admin');
    }
  }
  ngOnInit() {
    this.router.events
      .filter((event) => event instanceof ActivationEnd)
      .subscribe((_) => {
        this.outlogin = localStorage.getItem('token_admin') === null;
      });
  }
  logout() {
    localStorage.removeItem('token_admin');
    this.router.navigate(['login']);
  }
}
