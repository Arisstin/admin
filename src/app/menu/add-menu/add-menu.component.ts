import { Component, OnInit } from '@angular/core';
import {Page} from './add-menu.classes';
import {InfoServices} from '../../services/info.services';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.scss']
})
export class AddMenuComponent implements OnInit {
  list: Page[];
  constructor(private infoservices: InfoServices) {
    this.infoservices.getInfoPagesList().subscribe(res => {
      this.list = res;
    });
  }

  ngOnInit() {
  }

}
